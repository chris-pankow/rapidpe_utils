import sys
import os
import re

import numpy

import h5py

from glue.lal import Cache, CacheEntry
from glue.ligolw import utils, lsctables, ligolw
lsctables.use_in(ligolw.LIGOLWContentHandler)

def parse_intr_prms(prms):
    pdict = dict(p.split("=") for p in prms)
    for k in pdict:
        pdict[k] = float(pdict[k])
    return pdict

def read_bank(bankf):
    xmldoc = utils.load_filename(bankf, contenthandler=ligolw.LIGOLWContentHandler)
    try:
        tmplt_bank = lsctables.SnglInspiralTable.get_table(xmldoc)
    except ValueError:
        tmplt_bank = lsctables.SimInspiralTable.get_table(xmldoc)
    return tmplt_bank

def bank_to_pts(bank, intr_prms):
    return numpy.asarray([[getattr(t, p) for p in intr_prms] for t in bank])

def bank_to_ids(bank):
    return numpy.asarray([int(t.event_id) for t in bank])

def map_subbank(bankf):
    xmldoc = utils.load_filename(bankf, contenthandler=ligolw.LIGOLWContentHandler)
    bank = lsctables.SnglInspiralTable.get_table(xmldoc)
    return {bankf: map(int, [e.event_id for e in bank])}

def map_tmplt_to_subbank(tid, bnkmap):
    return filter(lambda b: tid in bnkmap[b], bnkmap.keys())

def concatenate_overlaps(files, outfile):
    struct = {}
    for f in files:
        src = h5py.File(f, "r")
        for grp in src.keys():
            if grp not in struct:
                dst = struct[grp] = {}
            else:
                dst = struct[grp]

            for name, dset in src[grp].iteritems():
                if name not in dst:
                    dst[name] = dset[:]
                else:
                    if name == "overlaps":
                        import scipy.sparse
                        dst[name] = scipy.sparse.block_diag((dst[name], dset))
                    else:
                        dst[name] = numpy.concatenate((dst[name], dset[:]))
        src.close()

    h5file = h5py.File(outfile, "w")
    for g, dset in struct.iteritems():
        grp = h5file.create_group(g)
        for nm, dat in dset.iteritems():
            if not isinstance(dat, numpy.ndarray):
                grp.create_dataset(nm, data=dat.toarray(), dtype=numpy.float32)
            else:
                grp.create_dataset(nm, data=dat, dtype=numpy.float32)
    h5file.close()

def load_overlap_id_data(olap_file, wfrm_fam=None, verbose=False):
    h5file = h5py.File(olap_file, "r")
    wfrm_fam = wfrm_fam or h5file.keys()[0]
    odata = h5file[wfrm_fam]
    id1, id2 = odata["id"][:], odata["id2"][:]
    h5file.close()

    return id1, id2

def load_overlap_metadata(olap_file, intr_prms=None, wfrm_fam=None, verbose=False):
    if verbose:
        print "Loading HDF5 overlap bank from " + olap_file
    h5file = h5py.File(olap_file, "r")

    # Just get the first one
    wfrm_fam = wfrm_fam or h5file.keys()[0]
    if verbose:
        print "Retrieving overlaps for waveform family: %s" % wfrm_fam

    odata = h5file[wfrm_fam]
    shp = odata["overlaps"].shape
    if verbose:
        print "Loaded (%d x %d) overlap matrix" % shp

    data = {}
    for i in intr_prms:
        if i not in odata:
            h5file.close()
            raise ValueError("Parameter %s requested, but not found in the overlap bank." % i)
        data[i] = odata[i][:]

    # FIXME: Need to close file, but copying overlap matrix is expensive
    h5file.close()
    return shp, data

def find_pt_exact(in_pt_ar, params):
    idx = None
    #for i in range(shp[1]):
    for i in range(params.shape[1]):
        # FIXME: Explicit list cast because the indexed recarrays return records
        # which don't have good comparison properties and no obvious way to get
        # them back to regular numpy arrays
        if numpy.allclose(list(params[i]), in_pt_ar):
            idx = i
            break
    return idx

def find_pt_inexact(in_pt_at, tmplts, tree=None):
    from sklearn.neighbors import BallTree
    if tree is None:
        tree = BallTree(tmplts)
    idx = tree.query(numpy.atleast_2d(in_pt_at), return_distance=False)[0][0]
    return idx, tree

if __name__ == "__main__":

    #in_pt = parse_intr_prms(args.intrinsic_param)
    #in_pt = parse_intr_prms(intrinsic_param)

    uberbank_file = "../etc/H1L1-UBERBANK_MAXM100_NS0p05_ER8HMPSD-1126033217-223200.xml.gz"

    # Get our listing of split banks
    bank_caches = "../etc/O1_uberbank_split_bank.cache"
    split_bank_files = [ce.path for ce in Cache.fromfile(open(bank_caches))]
    subbank_mapping = {}
    for split_bank in split_bank_files:
        subbank_mapping.update(map_subbank(split_bank))

    intr_prms = ("spin1z", "spin2z", "mass1", "mass2")
    full_bank = read_bank(uberbank_file)
    full_bank_ids = bank_to_ids(full_bank)
    full_bank = bank_to_pts(full_bank, intr_prms)
    print "Loaded bank with %d x %d entries" % full_bank.shape

    # Get our listing of overlaps
    overlap_cache = "../etc/O1_uberbank_overlaps.cache"
    bank_files = [ce.path for ce in Cache.fromfile(open(overlap_cache))]
    print "Will check %d bank files" % len(bank_files)

    tmplt_indx_mapping = {}
    tree = None
    for in_pt, in_pt_id in zip(full_bank, full_bank_ids):
        #in_pt = numpy.asarray([getattr(tmplt, a) for a in intr_prms])
        tmplt_idx, tree = find_pt_inexact(in_pt, full_bank, tree)

        for bankf, bankids in subbank_mapping.iteritems():
            if tmplt_idx not in bankids:
                continue

            # Map to the overlap file via ID
            bank_id = re.search("([0-9]{4})", bankf).group(1)
            bank_id = "_" + str(int(bank_id)) + "_"
            split_bank_overlap_file = filter(lambda b: bank_id in b, bank_files)
            if len(split_bank_overlap_file) == 0:
                print >>sys.stderr, "Uhhh... you have a problem."
                exit()
            split_bank_overlap_file = split_bank_overlap_file[0]

            # Check if the id is present
            id1, _ = load_overlap_id_data(split_bank_overlap_file, verbose=True)
            if not any(id1 == in_pt_id):
                continue

            # FIXME need the template ID
            tmplt_indx_mapping[in_pt_id] = split_bank_overlap_file
            break

        if in_pt_id not in tmplt_indx_mapping:
            print "No mapping found for %d"  % in_pt_id
            continue

        print in_pt_id, tmplt_indx_mapping[in_pt_id]

    import json
    with open("tmplt_to_O1_uber_overlap.json", "w") as fout:
        json.dump(tmplt_indx_mapping, fout)

    exit()

    #
    # Testing
    #
    intrinsic_param = ("mass1=1.0", "mass2=1.0", "spin1z=0.0", "spin2z=0.0")
    tmplt_idx, tree = find_pt_inexact(in_pt.values(), full_bank, tree)

    exit()

    #
    # Still needed?
    #

    for bank_file in bank_files:
        #if args.verbose:
        "Checking %s" % bank_file

        shp, params = load_overlap_metadata(bank_file, in_pt.keys(), verbose=True)
        # Convert to recarray
        params = numpy.rec.fromarrays(params.values(), names=",".join(params.keys()))

        # FIXME: Make option for 'fail on point not in bank'
        # Find the correct index
        idx = find_pt_exact(in_pt.values(), params)

        # We have a rectangular matrix, and the indexing is going to be off, we have
        # to remap it
        # NOTE: This implies two additional conditions about the overlap matrix
        # 1. The *same* bank has been used in both rows and columns. This has sorta
        # always been true, but it's required now. 
        # 2. Whatever template the row corresponds to is also represented in the
        # columns
        #  - If this isn't true, then there's no way to retrieve the intrinsic
        #    parameters of this row since we only keep one list of them, mapped to
        #    cols

        if idx is None:
            continue

        print "Located point in bank %s" % bank_file

        #id1, id2 = load_overlap_id_data(bank_file, verbose=args.verbose)
        id1, id2 = load_overlap_id_data(bank_file, verbose=True)
        if not any(id2[idx] == id1):
            # This means the overlap with another template was calculated and
            # we're close by, but this doesn't have the point itself in the bank
            # FIXME: Does this contain additional information we can use later?
            print "...but this is not the right bank."
            continue

        idx = numpy.argwhere(id2[idx] == id1)[0][0]
        print idx

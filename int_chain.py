import math

import numpy

from glue.ligolw import lsctables, ligolw
lsctables.use_in(ligolw.LIGOLWContentHandler)

class IntegralChain(object):
    def __init__(self):
        self.__chainid = 0
        self.chains = {}
        self.__sortval = {}
        self.__chainids = []

        self.ntotal = {}
        self.neff = {}
        self.integral_value = {}
        self.relative_error = {}

    def add_chains(self, xmldoc):
        """
        Add the results and chains (if present) from an XML doc. The chain is just the array of samples (SimInspiral table), and the results are the SnglBurst table. The indexing of samples will be handled internally so as to ensure proper bookkeeping of omitted samples. Returns the chain id assigned to this chain.
        FIXME: This will not quite work with merged files, because the simulation IDs are changed to avoid collisions.
        """
        results = lsctables.SnglInspiralTable.get_table(xmldoc)
        try:
            samples = lsctables.SimInspiralTable.get_table(xmldoc)
        except ValueError:
            samples = []

        for result in results:
            lastid = self.add_chain(result, filter(lambda s: s.process_id == result.process_id, samples))
        return lastid

    def add_chain(self, result, samples=None):
        """
        Add the results and chains (if present) from an XML doc. The chain is just the array of samples (SimInspiral table), and the results are the SnglBurst table. The indexing of samples will be handled internally so as to ensure proper bookkeeping of omitted samples. Returns the chain id assigned to this chain.
        """
        self.ntotal[self.__chainid] = result.ttotal
        self.neff[self.__chainid] = result.tau0
        self.integral_value[self.__chainid] = math.exp(result.snr)
        self.relative_error[self.__chainid] = result.event_duration

        if samples:
            self.chains[self.__chainid] = samples
            self.__sortval[self.__chainid] = numpy.argsort([int(samp.simulation_id) for samp in self.chains[self.__chainid]])
        else:
            self.chains[self.__chainid] = []
            self.__sortval[self.__chainid] = []
        self.__chainids.append(self.__chainid)
        self.__chainid += 1
        return self.__chainids[-1]

    def func_vals(self):
        """
        Get a normalized list of the integrand values. A numpy array of shape (nchains, max_length) is returned, and omitted samples are assigned the value zero.
        """
        max_len = max(self.ntotal.values())
        out_array = numpy.zeros( (self.__chainid, max_len+1) )
        for cid, idxes in self.__sortval.iteritems():
            for idx in idxes:
                itr_n = int(self.chains[cid][idx].simulation_id)
                out_array[cid, itr_n] = math.exp(self.chains[cid][idx].alpha1)
        return out_array

    def int_vals(self, total=False):
        """
        Get a normalized list of the point integral values. A numpy array of shape (nchains, max_length) is returned, and omitted samples are assigned the value zero.
        """
        max_len = max(self.ntotal.values())
        out_array = numpy.zeros( (self.__chainid, max_len+1) )
        for cid, idxes in self.__sortval.iteritems():
            for i, idx in enumerate(idxes):
                itr_n = int(self.chains[cid][idx].simulation_id)
                out_array[cid, itr_n] = math.exp(self.chains[cid][idx].alpha1)*self.chains[cid][idx].alpha2/self.chains[cid][idx].alpha3

        if not total:
            return out_array
        else:
            return out_array.sum(axis=0).cumsum()/numpy.arange(1, max_len+2)/self.__chainid

    def samp_idx(self, cid):
        """
        Get the internal indexing for chain with ID cid.
        """
        return self.__sortval[cid]

    def chain_ids(self):
        """
        Generator for chain ids.
        """
        for c in self.__chainids:
            yield c

    def total_evidence(self):
        """
        Return the evidence as calculated across all chains.
        """
        evid, ntot = 0, 0
        for cid in self.chain_ids():
            evid += self.integral_value[cid]*self.ntotal[cid]
            ntot += self.ntotal[cid]
        evid = evid/ntot
        return evid

import glob
import math
from collections import defaultdict

import numpy
from numpy.lib.recfunctions import append_fields, stack_arrays
from sklearn.neighbors import BallTree

from glue.ligolw import lsctables, utils, ligolw
lsctables.use_in(ligolw.LIGOLWContentHandler)

from lalinference.rapid_pe import lalsimutils

# We've never done any more than 10 *identical* runs, and we very rarely do more
# than 1 instance any more, but just in case...
_SEARCH_WINDOW = 10

INTR_PRMS = ("mass1", "mass2", "spin1x", "spin1y", "spin1z", "spin2x", "spin2y", "spin2z")
INTR_PRMS_DERIVED = ("mchirp", "eta", "chi_z")
EXTR_PRMS = ("inclination", "distance", "polarization", "coa_phase", "latitude", "longitude", "alpha1", "alpha2", "alpha3")
EXTR_MAP = dict(zip(EXTR_PRMS, ("inclination", "distance", "polarization", "coa_phase", "declination", "right_ascension", "logl", "prior", "samp_f")))

def chi_z(m1, m2, s1z, s2z):
    """
    Calculate dimensionless aligned spin parameter.
    """
    return (m1 * s1z + m2 * s2z) / (m1 + m2)

def runs_to_array(runs, attrs, asrecarray=True):
    """
    Convert runs to a record array, including dimensions indicated in `attrs'.
    """
    if asrecarray:
        return numpy.rec.fromarrays([[getattr(r, a) for r in runs] for a in attrs], names=",".join(attrs))
    else:
        return numpy.array([[getattr(r, a) for a in attrs] for r in runs])

def merge_runs(runs, prms=None, verbose=False):
    # We need a distance tree to query against so as not to incur an "allclose"
    # check against every point *in* the grid *for* every point in the grid
    run_pt_idx = runs_to_array(runs, prms or INTR_PRMS, False)
    tree = BallTree(run_pt_idx)
    if verbose:
        print "Tree constructed with %d points in %d dimensions" % tree.data.shape

    merged_runs = []
    pts = []

    for i, pt in enumerate(run_pt_idx):

        # Check if the point has already been accounted for. I don't think this
        # is possible, we might try cutting this out.
        try:
            pt_idx = pts.index(pt)
        except ValueError:
            pt_idx = None

        # Point already exists in dictionary, add evidences
        if pt_idx is not None:
            if verbose:
                print "Point %d (%s) found in grid already, adding evidence" % (i, ", ".join(map(str, pt)))

            runs[pt_idx].add_run(runs[i])
            merged_runs.append(i)
            continue

        # Check if there is another 'nearby' grid point, if so, add to that
        pt2_idx = tree.query(pt, k=_SEARCH_WINDOW, return_distance=False)[0]

        # Exclude self
        # FIXME: Use distance of == 0.0 as criteria instead
        merged = False
        for pt2_idx in pt2_idx[pt2_idx != i]:
            pt2 = tuple(run_pt_idx[pt2_idx])
            if numpy.allclose(pt, pt2) and (pt2_idx in pts or pt2_idx in merged_runs):
                if verbose:
                    print "Point %d (%s) near grid point (%s), merging runs" % (i, ", ".join(map(str, pt)), ", ".join(map(str, pt2)))

                runs[pt2_idx].add_run(runs[i], marg=set(prms))
                merged_runs.append(i)
                merged = True
                break

        # We found a grid point for this already, move on
        if merged:
            continue

        if verbose:
            #import pdb; pdb.set_trace()
            print "Point %d (%s) is NOT near any grid point, closest is (%s), appending" % (i, ", ".join(map(str, pt)), ", ".join(map(str, pt2)))
        pts.append(i)

    for i in sorted(merged_runs)[::-1]:
        del runs[i]

    return runs

def integrate(subgrid, loge, keep_idx, verbose=False):
    # We need a distance tree to query against so as not to incur an "allclose"
    # check against every point in the grid *for* every point in the grid
    tree = BallTree(subgrid[:,keep_idx])

    logevidence = defaultdict(list)
    subpts = defaultdict(list)
    for i, gpt in enumerate(subgrid):

        # FIXME: This number is arbitrary
        pt = numpy.round(gpt[keep_idx], 6)

        # Point already exists in dictionary, add evidences
        if tuple(pt) in logevidence:
            if verbose:
                print "Point %d (%s) found in grid already, appending" % (i, ", ".join(map(str, pt)))

            logevidence[tuple(pt)].append(loge[i])
            subpts[tuple(pt)].append(i)
            continue

        # Check if there is another 'nearby' grid point, if so, add to that
        pt2_idx = tree.query(pt, k=2, return_distance=False)[0]
        pt2_idx = pt2_idx[pt2_idx != i][0]
        pt2 = tuple(subgrid[pt2_idx][keep_idx])
        if numpy.allclose(pt, pt2) and pt2 in logevidence:
            if verbose:
                print "Point %d (%s) near grid point (%s), appending" % (i, ", ".join(map(str, pt)), ", ".join(map(str, pt2)))
            logevidence[pt2].append(loge[i])
            subpts[tuple(pt2)].append(i)
            continue

        # Otherwise, just append the point to the dictionary
        if verbose:
            #print numpy.allclose(pt, pt2), pt2 in logevidence
            print ">>>>>>"
            print tree.query(pt, k=4, return_distance=True)
            print "Point %d (%s) is NOT near any grid point, closest is point %d (%s), appending" % (i, ", ".join(map(str, pt)), pt2_idx, ", ".join(map(str, pt2)))
        logevidence[tuple(pt)].append(loge[i])
        subpts[tuple(pt)].append(i)

    if verbose:
        print "Subgrid has %d points" % len(logevidence)

    # Get the full extent of the grid points we've evaluated
    marg_mask = numpy.asarray(list(set(numpy.arange(subgrid.shape[-1])) - set(keep_idx)))
    delete_pts = []
    for pt, loge in logevidence.iteritems():
        if verbose:
            print "Marginalizing at fixed point (%s)" % ", ".join(map(str, pt))
        int_pts = subgrid[numpy.asarray(subpts[pt])][:,marg_mask]

        # Hacky solution: average over the volume
        from lalinference.rapid_pe import amrlib
        # This is the "integration volume"
        vol = amrlib.Cell.make_cell_from_boundaries(None, int_pts, False)

        # FIXME: what happens when any of the dimensions are singular?
        if vol.area() == 0:
            if verbose:
                print "Point %s has no subvolume, cannot be integrated." % ", ".join(map(str, pt))
            delete_pts.append(pt)
            continue
        elif verbose:
            print "Integrating over %d points" % len(int_pts)
        # This is the log of the evidence integral over the region
        logevidence[pt] = add_logevid_many(loge) + math.log(vol.area())

        if verbose:
            print "\tnpts %d, logmax %1.2e, logmin %1.2e, vol %1.2e, extent %s" % (len(int_pts), max(loge), min(loge), vol.area(), str(vol._bounds))
            print "log integral: %1.2e" % logevidence[pt]


    for pt in delete_pts:
        del logevidence[pt]

    return logevidence

def marginalize(runs, attrs, marg, fudge=None, verbose=False):
    """
    Marginalize runs in all dimensions not specified in `attrs'. Duplicate runs (runs with the same intrinsic parameters) will be merged automatically.
    FIXME: `fudge' parameter is currently ignored, and "close by" points are merged automatically.
    """
    # FIXME: Won't work for derived parameters
    #prms = list(INTR_PRMS) + list(INTR_PRMS_DERIVED)
    prms = list(attrs) + ["logevidence", "ntotal"]

    if verbose:
        print "Retrieving parameters: %s" % ", ".join(prms)

    # oonvert runs to grid and get logevidence and total points used
    grid = runs_to_array(runs, prms)
    loge = grid.logevidence
    ntotal = grid.ntotal

    """
    # No dimensions specified, return total evidence
    if attrs is None:
        if verbose:
            print "No marginalization parameters supplied, returning total ", \
                    "marginalized evidence for this run"
        return add_logevid_many(loge, ntotal)
    """

    # generate subgrid to merge together and marginalize over other parameters
    subgrid = []
    for a in attrs:
        subgrid.append(grid[a])
    subgrid = numpy.atleast_2d(subgrid).T

    if verbose:
        print "Subgrid has initial shape: %s" % str(subgrid.shape)

    return integrate(subgrid, loge, verbose)

def get_dim_slices(dim, fudge=None):
    return numpy.unique(dim)

def add_logevid(levid1, levid2, n1, n2):
    """
    Add together the evidences in the log, taking into account the number of points for each run.
    """
    levid1 += math.log(n1)
    levid2 += math.log(n2)
    total = numpy.logaddexp(levid1, levid2)
    return total - math.log(n1 + n2)

def add_logevid_many(levid, n=None):
    if n is None:
        n = numpy.ones(len(levid))
    else:
        levid += numpy.log(n)
    total = numpy.logaddexp.reduce(levid)
    return total - math.log(numpy.sum(n))

def add_errors(rerr1, levid1, rerr2, levid2):
    levid1 += numpy.log(rerr1)
    levid2 += numpy.log(rerr2)
    rerr1 = numpy.logaddexp(2 * levid1, 2 * levid2)
    return math.sqrt(math.exp(rerr1))

def add_neff(levid1, levid2, n1, n2, neff1, neff2):
    total = add_logevid(levid1, levid2, n1, n2)
    levid1 += math.log(n1/neff1)
    levid2 += math.log(n2/neff2)
    logmaxw = max(levid1, levid2)
    return math.exp(total + math.log(n1 + n2) - logmaxw)

def runs_from_glob(glb="*.xml.gz"):
    return map(RapidPERun, filter(lambda s: "samples" not in s, glob.glob(glb)))

def runs_from_xml(fname):
    xmldoc = utils.load_filename(fname, contenthandler=ligolw.LIGOLWContentHandler)
    sngl_table = lsctables.SnglInspiralTable.get_table(xmldoc)
    runs = []
    for s in sngl_table:
        run = RapidPERun()
        run.load_run(s)
        runs.append(run)
    return runs

class RapidPERun(object):
    def __init__(self, fname=None):
        self.logevidence = 0
        self.ntotal = 0
        self.neff = 0
        self.rel_error = 0

        # Primary fields
        self.mass1, self.mass2 = 0, 0
        self.spin1x, self.spin1y, self.spin1z = 0, 0, 0
        self.spin2x, self.spin2y, self.spin2z = 0, 0, 0

        # Derived fields
        self.mchirp, self.eta = 0, 0
        self.chi_z = 0

        self.samples = None

        if fname is not None:
            self.load_run(fname)

    # FIXME: Probably a way to overload the + operator
    def add_run(self, fname, marg=set()):
        if isinstance(fname, RapidPERun):
            run2 = fname
        else:
            run2 = RapidPERun()
            run2.load_run(fname)

        iprms = numpy.array([(getattr(self, a), getattr(run2, a)) for a in marg])
        # FIXME: What do we do with the other parameters?
        assert numpy.allclose(iprms[:,0], iprms[:,1])

        #self.rel_error = add_errors(self.rel_error, self.logevidence, run2.rel_error, run2.logevidence)
        # FIXME: Not really right
        self.rel_error = min(self.rel_error, run2.rel_error)
        self.logevidence = add_logevid(self.logevidence, run2.logevidence, self.ntotal, run2.ntotal)
        self.ntotal += run2.ntotal
        self.neff = add_neff(self.logevidence, run2.logevidence, self.ntotal, run2.ntotal, self.neff, run2.neff)
        self.samples = stack_arrays((self.samples, run2.samples), asrecarray=True, usemask=False)

    def load_run(self, fname):
        xmldoc, _samples = None, None
        if not isinstance(fname, str):
            run_table = fname
        else:
            xmldoc = utils.load_filename(fname, contenthandler=ligolw.LIGOLWContentHandler)
            run_table = lsctables.SnglInspiralTable.get_table(xmldoc)[0]
            try:
                _samples = lsctables.SimInspiralTable.get_table(xmldoc)
            except ValueError:
                pass

        for a in INTR_PRMS:
            try:
                setattr(self, a, getattr(run_table, a))
            except AttributeError:
                pass
        # FIXME: Make this more programmatic
        self.mchirp, self.eta = lalsimutils.Mceta(self.mass1, self.mass2)
        self.chi_z = chi_z(self.mass1, self.mass2, self.spin1z, self.spin2z)

        self.logevidence = run_table.snr
        self.ntotal = run_table.ttotal
        self.neff = run_table.tau0
        self.rel_error = run_table.event_duration

        if _samples is not None:
            pnames = ",".join([EXTR_MAP[p] for p in EXTR_PRMS])
            self.samples = numpy.rec.fromarrays([[getattr(samp, a) for samp in _samples] for a in EXTR_PRMS], names=pnames)
            self.samples = append_fields(self.samples, "weights", numpy.exp(self.samples.logl) * self.samples.prior / self.samples.samp_f, asrecarray=True)
            self.samples = append_fields(self.samples, "n", map(int, [s.simulation_id for s in _samples]), asrecarray=True, usemask=False)
            del _samples

        if xmldoc is not None:
            xmldoc.unlink()
